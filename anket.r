library(ggplot2)
library(tidyr)

anket <- read.csv("anket.csv")
anket$Tarih <- as.Date(anket$Tarih, format = "%d-%m-%Y")

anket %>%
  tidyr::pivot_longer(-Tarih) %>%
  ggplot() +
  aes(Tarih, value,
      color =
        name) +
  geom_point() +
  geom_smooth(se = FALSE, show.legend = FALSE) +
  scale_colour_manual(
    values = c(
      # AKP
      "#ff8700",
      # CHP
      "#ff0000",
      # DEVA
      "#006d9e",
      # GP
      "#2db34a",
      # HDP
      "#8000ff",
      # MHP
      "#e60019",
      # MP
      "#0d5da6",
      # SP
      "#ff0003",
      # İYİ
      "#3db5e6"
    )
  ) +
  scale_x_date(date_breaks = "3 months", date_labels = "%m/%Y") +
  labs(x = "", y = "") +
  theme(
    axis.text.x = element_text(size = 20),
    axis.text.y = element_text(size = 20),
    legend.position = "bottom",
    legend.key = element_blank(),
    legend.text = element_text(size = 15)
  ) +
  guides(col = guide_legend(
    nrow = 1,
    title = NULL,
    override.aes = list(size = 3)
  ))

